#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <algorithm>

#include "Graph.h"

using namespace std;

Graph::Graph(){
  m = -1;
  L = -1;
}

Graph::~Graph(){
  std::vector<Node*>::iterator it = nodeList->begin();
  for(; it != nodeList->end(); ++it)
    delete *it;

  delete nodeList;
}


bool Graph::compByK(Node *a, Node *b){
  return (int)a->getK() > (int)b->getK();
}


Node * Graph::findNode(const std::string& name) {
  for (int i = 0; i < nodeList->size(); i++) {
    std::string s = nodeList->at(i)->getName(); 
    if (s.compare(name) == 0)
	return nodeList->at(i);
  }
  return NULL;
}


bool Graph::readGraph(const std::string& file){


  //Read the graph.
  std::ifstream str(file.c_str());
  if (str.is_open()) {

    nodeList =  new std::vector<Node*>();
      
    std::string line; 
    int vertex1, vertex2;
    while (getline(str,line)) {
      
      std::istringstream ss(line);
      std::string vertex1, vertex2;
      ss >> vertex1;
      ss >> vertex2;

      Node *u = findNode(vertex1);
      if (u == NULL) {
	u = new Node(vertex1);
	addNewNode(u);
      }
      
      Node *v = findNode(vertex2);
      if (v == NULL) {
	v = new Node(vertex2);
	addNewNode(v);
      }
      
      u->addAdjNode(v);
      v->addAdjNode(u);
    }
    str.close();
  }

  // sort by degree
  std::sort(nodeList->begin(), nodeList->end(), compByK);
  
  // Calculate L and m
  int totalK = 0;
  for(int i = 0; i < nodeList->size(); ++i)
    totalK += nodeList->at(i)->getK();
  kbar = (double)totalK / nodeList->size();

  if(m == -1) // if not specified by the user, set it equal to the minimum node degree.
    m = nodeList->at(nodeList->size()-1)->getK();
  if(m < 0) {
    cout<<"Warning: setting m = 0.\n";
    m = 0;
  }

  if(L == -1) // if not specified by the user.
    L = (kbar - 2.0 * m) / 2.0;
  if(L < 0) {
    cout<<"Warning: setting L = 0.\n";
    L = 0;
  }

  return true;
}


void Graph::print2file(const std::string& file){
  // Format: node_id angular_coordinate radial_coordinate.
  std::ofstream out;
  out.precision(15);
  out.open(file.c_str());
  for (int i = 0; i < nodeList->size(); ++i) {
    Node *u = nodeList->at(i);
    out << u->getName() << " "
	<< u->getAngle() << " " << u->getRadius() << "\n";
  }
  out.close();
}
