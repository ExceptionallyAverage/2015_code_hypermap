CC=g++
CFLAGS=-Wall -O3
SOURCES=src/hypermap.cpp src/Graph.cpp
OBJECTS:=src/hypermap.o src/Graph.o
HEADERS=src/*.h
TORELEASE=src AS_topologies README contributors.txt

all: hypermap

hypermap: $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^

src/%.o: %.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c $<

release:
	make cleanest
	tar cvzf HypermapCN.tar.gz src AS_topologies README
clean:
	rm $(OBJECTS)

cleanest: clean
	rm hypermap
